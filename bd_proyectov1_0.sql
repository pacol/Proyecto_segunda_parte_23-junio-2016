-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-06-2016 a las 06:32:01
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bd_proyectov1.0`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actareunion`
--

CREATE TABLE IF NOT EXISTS `actareunion` (
  `id_actareunion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_finalizacion` time DEFAULT NULL,
  `lugar` varchar(50) DEFAULT NULL,
  `proyecto` varchar(100) DEFAULT NULL,
  `asusto` varchar(50) DEFAULT NULL,
  `asistentes` varchar(200) DEFAULT NULL,
  `asistentes_externos` varchar(200) DEFAULT NULL,
  `elaborado_por` varchar(100) DEFAULT NULL,
  `proxima_reunion` varchar(50) DEFAULT NULL,
  `Descripcion_reunion` varchar(500) DEFAULT NULL,
  `fk_idprofesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_actareunion`),
  KEY `actareunionprofesor` (`fk_idprofesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE IF NOT EXISTS `aprendiz` (
  `id_aprendiz` bigint(20) NOT NULL AUTO_INCREMENT,
  `foto_aprendiz` blob NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `documento` bigint(20) NOT NULL,
  `edad` bigint(20) DEFAULT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `Correo` varchar(100) DEFAULT NULL,
  `fkid_especialidad` bigint(20) DEFAULT NULL,
  `fk_idestado` bigint(20) DEFAULT NULL,
  `fkid_tipodocumento` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_aprendiz`),
  KEY `aprendizespecialidad` (`fkid_especialidad`),
  KEY `aprendizestado` (`fk_idestado`),
  KEY `aprendiztipodocumento` (`fkid_tipodocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE IF NOT EXISTS `archivo` (
  `id_archivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fk_idprofesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_archivo`),
  KEY `archivoprofesor` (`fk_idprofesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistenciapasantia`
--

CREATE TABLE IF NOT EXISTS `asistenciapasantia` (
  `id_AsistenciaPasantia` bigint(20) NOT NULL AUTO_INCREMENT,
  `fk_idaprendiz` bigint(20) DEFAULT NULL,
  `asistencia` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora_ingreso` time DEFAULT NULL,
  `hora_egreso` time DEFAULT NULL,
  `firma_responsable` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_AsistenciaPasantia`),
  KEY `asistenciapasantiaaprendiz` (`fk_idaprendiz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistenciareunionespadres`
--

CREATE TABLE IF NOT EXISTS `asistenciareunionespadres` (
  `id_asistenciareunionpadres` bigint(20) NOT NULL AUTO_INCREMENT,
  `asamblea_general` varchar(50) DEFAULT NULL,
  `primera` varchar(50) DEFAULT NULL,
  `segunda` varchar(50) DEFAULT NULL,
  `tercera` varchar(50) DEFAULT NULL,
  `cuarta` varchar(50) DEFAULT NULL,
  `observaciones` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_asistenciareunionpadres`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `causas_desercion`
--

CREATE TABLE IF NOT EXISTS `causas_desercion` (
  `id_causas_desercion` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_causas_desercion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_causas_desercion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citacionpadres`
--

CREATE TABLE IF NOT EXISTS `citacionpadres` (
  `id_citacionpadres` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Epecialidad` bigint(20) DEFAULT NULL,
  `jornada` varchar(50) DEFAULT NULL,
  `sede` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `motivo` varchar(50) DEFAULT NULL,
  `firma_aprendiz` text,
  `firmaprofesor` text,
  PRIMARY KEY (`id_citacionpadres`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cronograma`
--

CREATE TABLE IF NOT EXISTS `cronograma` (
  `id_cronograma` bigint(20) NOT NULL AUTO_INCREMENT,
  `cronograma` varchar(50) DEFAULT NULL,
  `fkid_profesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_cronograma`),
  KEY `cronogramaprofesor` (`fkid_profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desercion`
--

CREATE TABLE IF NOT EXISTS `desercion` (
  `id_desercion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_inicio_inasistencia` date DEFAULT NULL,
  `fecha_desercion` date DEFAULT NULL,
  PRIMARY KEY (`id_desercion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleaprendizprofesor`
--

CREATE TABLE IF NOT EXISTS `detalleaprendizprofesor` (
  `id_detalleAprendizProfesor` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_aprendiz` bigint(20) DEFAULT NULL,
  `id_profesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalleAprendizProfesor`),
  KEY `detalleaprendizprofesoraprendiz` (`id_aprendiz`),
  KEY `detalleaprendizprofesorprofesor` (`id_profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleasisteciareunion_observador`
--

CREATE TABLE IF NOT EXISTS `detalleasisteciareunion_observador` (
  `id_asisteciaReunion_observador` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_asistenciareunion` bigint(20) DEFAULT NULL,
  `fkid_observador` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_asisteciaReunion_observador`),
  KEY `detalleasisteciareunion_observadorasistenciareunion` (`fkid_asistenciareunion`),
  KEY `detalleasisteciareunion_observador` (`fkid_observador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecausuasdesercion_reportedesercion`
--

CREATE TABLE IF NOT EXISTS `detallecausuasdesercion_reportedesercion` (
  `id_detallecausuasdesercion_reportedesercion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_desercion` bigint(20) DEFAULT NULL,
  `fkid_causasdesercion` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detallecausuasdesercion_reportedesercion`),
  KEY `detallecausuasdesercion_reportedeserciondesercion` (`fkid_desercion`),
  KEY `detallecausuasdesercion_reportedesercioncausasdesercion` (`fkid_causasdesercion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallecitacionpadres_observador`
--

CREATE TABLE IF NOT EXISTS `detallecitacionpadres_observador` (
  `id_detallecitacionpadres_observador` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_citacionpadres` bigint(20) DEFAULT NULL,
  `fkid_observador` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detallecitacionpadres_observador`),
  KEY `detallecitacionpadres_observadorcitacionpadres` (`fkid_citacionpadres`),
  KEY `detallecitacionpadres_observadorobservador` (`fkid_observador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleespecialidad_aprendiz`
--

CREATE TABLE IF NOT EXISTS `detalleespecialidad_aprendiz` (
  `id_detalleespecialidad_aprendiz` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_aprendiz` bigint(20) DEFAULT NULL,
  `fkid_especialidad` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalleespecialidad_aprendiz`),
  KEY `detalleespecialidad_aprendizaprendiz` (`fkid_aprendiz`),
  KEY `detalleespecialidad_aprendizespecialidad` (`fkid_especialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleinasistenciaaprendiz`
--

CREATE TABLE IF NOT EXISTS `detalleinasistenciaaprendiz` (
  `id_detalleInasistenciaAprendiz` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_aprendiz` bigint(20) DEFAULT NULL,
  `id_inasistencia` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalleInasistenciaAprendiz`),
  KEY `detalleinasistenciaaprendizaprendiz` (`id_aprendiz`),
  KEY `detalleinasistenciaaprendizinasistencia` (`id_inasistencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallereportedesercion_archivo`
--

CREATE TABLE IF NOT EXISTS `detallereportedesercion_archivo` (
  `id_reportedesercion_archivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_reportedesercion` bigint(20) DEFAULT NULL,
  `fkid_archivo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_reportedesercion_archivo`),
  KEY `detallereportedesercion_archivoreportedesercion` (`fkid_reportedesercion`),
  KEY `detallereportedesercion_archivoarchivo` (`fkid_archivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_actareunion_archivo`
--

CREATE TABLE IF NOT EXISTS `detalle_actareunion_archivo` (
  `id_detalle_actareunion_archivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_actareunion` bigint(20) DEFAULT NULL,
  `fkid_archivo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalle_actareunion_archivo`),
  KEY `detalle_actareunion_archivoactareunion` (`fkid_actareunion`),
  KEY `detalle_actareunion_archivoarchivo` (`fkid_archivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_asistenciapasantia_archivo`
--

CREATE TABLE IF NOT EXISTS `detalle_asistenciapasantia_archivo` (
  `id_detalle_asistenciapasantia_archivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_asistenciapasantia_archivo` bigint(20) DEFAULT NULL,
  `fkid_archivo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalle_asistenciapasantia_archivo`),
  KEY `detalle_asistenciapasantia_archivoasistenciapasantia` (`fkid_asistenciapasantia_archivo`),
  KEY `detalle_asistenciapasantia_archivoarchivo` (`fkid_archivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_plandemejoramiento_archivo`
--

CREATE TABLE IF NOT EXISTS `detalle_plandemejoramiento_archivo` (
  `id_detalle_plandemejoramiento_archivo` bigint(20) NOT NULL AUTO_INCREMENT,
  `fkid_plandemejoramiento` bigint(20) DEFAULT NULL,
  `fkid_archivo` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_detalle_plandemejoramiento_archivo`),
  KEY `detalle_plandemejoramiento_archivoarchivo` (`fkid_archivo`),
  KEY `detalle_plandemejoramiento_archivoplandemejoramiento` (`fkid_plandemejoramiento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE IF NOT EXISTS `especialidad` (
  `id_especialidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_especialidad` varchar(50) NOT NULL,
  `cantidad_estudiantes` varchar(50) DEFAULT NULL,
  `id_estado` bigint(20) NOT NULL,
  PRIMARY KEY (`id_especialidad`),
  KEY `especialidadestado` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id_estado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_estado` varchar(50) NOT NULL,
  `fk_tipoestado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_estado`),
  KEY `estadotipoestado` (`fk_tipoestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarioespecialidad`
--

CREATE TABLE IF NOT EXISTS `horarioespecialidad` (
  `id_horarioespecialidad` bigint(20) NOT NULL AUTO_INCREMENT,
  `lunes` varchar(50) DEFAULT NULL,
  `martes` varchar(50) DEFAULT NULL,
  `miercoles` varchar(50) DEFAULT NULL,
  `jueves` varchar(50) DEFAULT NULL,
  `viernes` varchar(50) DEFAULT NULL,
  `sabado` varchar(50) DEFAULT NULL,
  `domingo` varchar(50) DEFAULT NULL,
  `id_profesor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_horarioespecialidad`),
  KEY `horarioespecialidadprofesor` (`id_profesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inasistencia`
--

CREATE TABLE IF NOT EXISTS `inasistencia` (
  `id_inasistencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id_inasistencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observador`
--

CREATE TABLE IF NOT EXISTS `observador` (
  `id_observador` bigint(20) NOT NULL AUTO_INCREMENT,
  `observacion` varchar(500) DEFAULT NULL,
  `compromiso` varchar(500) DEFAULT NULL,
  `fkid_aprendiz` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_observador`),
  KEY `observadoraprendiz` (`fkid_aprendiz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plandemejoramiento`
--

CREATE TABLE IF NOT EXISTS `plandemejoramiento` (
  `id_plandemejoramiento` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_Especialidad` bigint(20) DEFAULT NULL,
  `id_profesor` bigint(20) DEFAULT NULL,
  `periodo_escolar` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `año` varchar(50) DEFAULT NULL,
  `fecha_plandemejoramiento` date DEFAULT NULL,
  `numero_ficha` varchar(50) DEFAULT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `id_Estudiante` bigint(20) DEFAULT NULL,
  `linea_de_trabajo` varchar(50) DEFAULT NULL,
  `nota_anterior` varchar(50) DEFAULT NULL,
  `nota_final` varchar(50) DEFAULT NULL,
  `juicio_De_valor` varchar(50) DEFAULT NULL,
  `firma_profesor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_plandemejoramiento`),
  KEY `plandemejoramientoespecialidad` (`id_Especialidad`),
  KEY `plandemejoramientoprofesor` (`id_profesor`),
  KEY `plandemejoramientoaprendiz` (`id_Estudiante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `documento` bigint(20) DEFAULT NULL,
  `id_tipoprofesor` bigint(20) DEFAULT NULL,
  `id_estado` bigint(20) DEFAULT NULL,
  `id_tipodocumento` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_profesor`),
  KEY `profesortipoprofesor` (`id_tipoprofesor`),
  KEY `profesorestado` (`id_estado`),
  KEY `profesortipo_documento` (`id_tipodocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportedesercion`
--

CREATE TABLE IF NOT EXISTS `reportedesercion` (
  `id_reportedesercion` bigint(20) NOT NULL AUTO_INCREMENT,
  `fk_idaprendiz` bigint(20) DEFAULT NULL,
  `id_especialidad` bigint(20) DEFAULT NULL,
  `regional` varchar(50) DEFAULT NULL,
  `nombre_centro_formacion_sena` varchar(100) DEFAULT NULL,
  `Numero_ficha` varchar(50) DEFAULT NULL,
  `resultado_aprendizaje` varchar(50) DEFAULT NULL,
  `fecha_inicio_resultado` date DEFAULT NULL,
  `Horario` varchar(50) DEFAULT NULL,
  `Empresa_patrocinadora` varchar(50) DEFAULT NULL,
  `fkid_desercion` bigint(20) DEFAULT NULL,
  `observaciones` varchar(500) DEFAULT NULL,
  `fecha_reporte` date DEFAULT NULL,
  `firma_instrutor` text,
  `firma_coordinacion_articulacion_sena` text,
  PRIMARY KEY (`id_reportedesercion`),
  KEY `reportedesercionaprendiz` (`fk_idaprendiz`),
  KEY `reportedesercionespecialidad` (`id_especialidad`),
  KEY `reportedeserciondesercion` (`fkid_desercion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoprofesor`
--

CREATE TABLE IF NOT EXISTS `tipoprofesor` (
  `id_tipoprofesor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tipoprofesor` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_tipoprofesor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE IF NOT EXISTS `tipo_documento` (
  `id_tipodocumento` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tipodocumento` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipodocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado`
--

CREATE TABLE IF NOT EXISTS `tipo_estado` (
  `id_tipoestado` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tipoestado` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipoestado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actareunion`
--
ALTER TABLE `actareunion`
  ADD CONSTRAINT `actareunion_ibfk_1` FOREIGN KEY (`fk_idprofesor`) REFERENCES `profesor` (`id_profesor`);

--
-- Filtros para la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `aprendiz_ibfk_3` FOREIGN KEY (`fkid_tipodocumento`) REFERENCES `tipo_documento` (`id_tipodocumento`),
  ADD CONSTRAINT `aprendiz_ibfk_1` FOREIGN KEY (`fkid_especialidad`) REFERENCES `especialidad` (`id_especialidad`),
  ADD CONSTRAINT `aprendiz_ibfk_2` FOREIGN KEY (`fk_idestado`) REFERENCES `estado` (`id_estado`);

--
-- Filtros para la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD CONSTRAINT `archivo_ibfk_1` FOREIGN KEY (`fk_idprofesor`) REFERENCES `profesor` (`id_profesor`);

--
-- Filtros para la tabla `asistenciapasantia`
--
ALTER TABLE `asistenciapasantia`
  ADD CONSTRAINT `asistenciapasantia_ibfk_1` FOREIGN KEY (`fk_idaprendiz`) REFERENCES `aprendiz` (`id_aprendiz`);

--
-- Filtros para la tabla `cronograma`
--
ALTER TABLE `cronograma`
  ADD CONSTRAINT `cronograma_ibfk_1` FOREIGN KEY (`fkid_profesor`) REFERENCES `profesor` (`id_profesor`);

--
-- Filtros para la tabla `detalleaprendizprofesor`
--
ALTER TABLE `detalleaprendizprofesor`
  ADD CONSTRAINT `detalleaprendizprofesor_ibfk_2` FOREIGN KEY (`id_profesor`) REFERENCES `profesor` (`id_profesor`),
  ADD CONSTRAINT `detalleaprendizprofesor_ibfk_1` FOREIGN KEY (`id_aprendiz`) REFERENCES `aprendiz` (`id_aprendiz`);

--
-- Filtros para la tabla `detalleasisteciareunion_observador`
--
ALTER TABLE `detalleasisteciareunion_observador`
  ADD CONSTRAINT `detalleasisteciareunion_observador_ibfk_2` FOREIGN KEY (`fkid_observador`) REFERENCES `observador` (`id_observador`),
  ADD CONSTRAINT `detalleasisteciareunion_observador_ibfk_1` FOREIGN KEY (`fkid_asistenciareunion`) REFERENCES `asistenciareunionespadres` (`id_asistenciareunionpadres`);

--
-- Filtros para la tabla `detallecausuasdesercion_reportedesercion`
--
ALTER TABLE `detallecausuasdesercion_reportedesercion`
  ADD CONSTRAINT `detallecausuasdesercion_reportedesercion_ibfk_2` FOREIGN KEY (`fkid_causasdesercion`) REFERENCES `causas_desercion` (`id_causas_desercion`),
  ADD CONSTRAINT `detallecausuasdesercion_reportedesercion_ibfk_1` FOREIGN KEY (`fkid_desercion`) REFERENCES `desercion` (`id_desercion`);

--
-- Filtros para la tabla `detallecitacionpadres_observador`
--
ALTER TABLE `detallecitacionpadres_observador`
  ADD CONSTRAINT `detallecitacionpadres_observador_ibfk_2` FOREIGN KEY (`fkid_observador`) REFERENCES `observador` (`id_observador`),
  ADD CONSTRAINT `detallecitacionpadres_observador_ibfk_1` FOREIGN KEY (`fkid_citacionpadres`) REFERENCES `citacionpadres` (`id_citacionpadres`);

--
-- Filtros para la tabla `detalleespecialidad_aprendiz`
--
ALTER TABLE `detalleespecialidad_aprendiz`
  ADD CONSTRAINT `detalleespecialidad_aprendiz_ibfk_2` FOREIGN KEY (`fkid_especialidad`) REFERENCES `especialidad` (`id_especialidad`),
  ADD CONSTRAINT `detalleespecialidad_aprendiz_ibfk_1` FOREIGN KEY (`fkid_aprendiz`) REFERENCES `aprendiz` (`id_aprendiz`);

--
-- Filtros para la tabla `detalleinasistenciaaprendiz`
--
ALTER TABLE `detalleinasistenciaaprendiz`
  ADD CONSTRAINT `detalleinasistenciaaprendiz_ibfk_2` FOREIGN KEY (`id_inasistencia`) REFERENCES `inasistencia` (`id_inasistencia`),
  ADD CONSTRAINT `detalleinasistenciaaprendiz_ibfk_1` FOREIGN KEY (`id_aprendiz`) REFERENCES `aprendiz` (`id_aprendiz`);

--
-- Filtros para la tabla `detallereportedesercion_archivo`
--
ALTER TABLE `detallereportedesercion_archivo`
  ADD CONSTRAINT `detallereportedesercion_archivo_ibfk_2` FOREIGN KEY (`fkid_archivo`) REFERENCES `archivo` (`id_archivo`),
  ADD CONSTRAINT `detallereportedesercion_archivo_ibfk_1` FOREIGN KEY (`fkid_reportedesercion`) REFERENCES `reportedesercion` (`id_reportedesercion`);

--
-- Filtros para la tabla `detalle_actareunion_archivo`
--
ALTER TABLE `detalle_actareunion_archivo`
  ADD CONSTRAINT `detalle_actareunion_archivo_ibfk_2` FOREIGN KEY (`fkid_archivo`) REFERENCES `archivo` (`id_archivo`),
  ADD CONSTRAINT `detalle_actareunion_archivo_ibfk_1` FOREIGN KEY (`fkid_actareunion`) REFERENCES `actareunion` (`id_actareunion`);

--
-- Filtros para la tabla `detalle_asistenciapasantia_archivo`
--
ALTER TABLE `detalle_asistenciapasantia_archivo`
  ADD CONSTRAINT `detalle_asistenciapasantia_archivo_ibfk_2` FOREIGN KEY (`fkid_archivo`) REFERENCES `archivo` (`id_archivo`),
  ADD CONSTRAINT `detalle_asistenciapasantia_archivo_ibfk_1` FOREIGN KEY (`fkid_asistenciapasantia_archivo`) REFERENCES `asistenciapasantia` (`id_AsistenciaPasantia`);

--
-- Filtros para la tabla `detalle_plandemejoramiento_archivo`
--
ALTER TABLE `detalle_plandemejoramiento_archivo`
  ADD CONSTRAINT `detalle_plandemejoramiento_archivo_ibfk_2` FOREIGN KEY (`fkid_plandemejoramiento`) REFERENCES `plandemejoramiento` (`id_plandemejoramiento`),
  ADD CONSTRAINT `detalle_plandemejoramiento_archivo_ibfk_1` FOREIGN KEY (`fkid_archivo`) REFERENCES `archivo` (`id_archivo`);

--
-- Filtros para la tabla `especialidad`
--
ALTER TABLE `especialidad`
  ADD CONSTRAINT `especialidad_ibfk_1` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`);

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `estado_ibfk_1` FOREIGN KEY (`fk_tipoestado`) REFERENCES `tipo_estado` (`id_tipoestado`);

--
-- Filtros para la tabla `horarioespecialidad`
--
ALTER TABLE `horarioespecialidad`
  ADD CONSTRAINT `horarioespecialidad_ibfk_1` FOREIGN KEY (`id_profesor`) REFERENCES `profesor` (`id_profesor`);

--
-- Filtros para la tabla `observador`
--
ALTER TABLE `observador`
  ADD CONSTRAINT `observador_ibfk_1` FOREIGN KEY (`fkid_aprendiz`) REFERENCES `aprendiz` (`id_aprendiz`);

--
-- Filtros para la tabla `plandemejoramiento`
--
ALTER TABLE `plandemejoramiento`
  ADD CONSTRAINT `plandemejoramiento_ibfk_3` FOREIGN KEY (`id_Estudiante`) REFERENCES `aprendiz` (`id_aprendiz`),
  ADD CONSTRAINT `plandemejoramiento_ibfk_1` FOREIGN KEY (`id_Especialidad`) REFERENCES `especialidad` (`id_especialidad`),
  ADD CONSTRAINT `plandemejoramiento_ibfk_2` FOREIGN KEY (`id_profesor`) REFERENCES `profesor` (`id_profesor`);

--
-- Filtros para la tabla `profesor`
--
ALTER TABLE `profesor`
  ADD CONSTRAINT `profesor_ibfk_3` FOREIGN KEY (`id_tipodocumento`) REFERENCES `tipo_documento` (`id_tipodocumento`),
  ADD CONSTRAINT `profesor_ibfk_1` FOREIGN KEY (`id_tipoprofesor`) REFERENCES `tipoprofesor` (`id_tipoprofesor`),
  ADD CONSTRAINT `profesor_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`);

--
-- Filtros para la tabla `reportedesercion`
--
ALTER TABLE `reportedesercion`
  ADD CONSTRAINT `reportedesercion_ibfk_3` FOREIGN KEY (`fkid_desercion`) REFERENCES `desercion` (`id_desercion`),
  ADD CONSTRAINT `reportedesercion_ibfk_1` FOREIGN KEY (`fk_idaprendiz`) REFERENCES `aprendiz` (`id_aprendiz`),
  ADD CONSTRAINT `reportedesercion_ibfk_2` FOREIGN KEY (`id_especialidad`) REFERENCES `especialidad` (`id_especialidad`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
